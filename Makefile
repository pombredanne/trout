SRC_DIR=	src
BIN_DIR=	bin

CXX=		/usr/bin/g++
CXXFLAGS=	-g -Wall

SRCS=		$(wildcard src/*.py)
TARGETS=	$(SRCS:$(SRC_DIR)/%.py=$(BIN_DIR)/%)

SAM_DATA=	sam/data/1_Actino16s.F.fq  sam/data/3_Actino16s.F.fq  sam/data/5_Actino16s.F.fq  sam/data/7_Actino16s.F.fq sam/data/2_Actino16s.F.fq  sam/data/4_Actino16s.F.fq  sam/data/6_Actino16s.F.fq  sam/data/8_Actino16s.F.fq  sam/data/Mock3_Actino16s.F.fq
SAM_MARK=	sam/data/act.lsh


all: trout

trout: $(BIN_DIR) $(BIN_DIR)/trout-suffix $(TARGETS)

$(BIN_DIR):
	mkdir -p $(BIN_DIR)

$(BIN_DIR)/trout-suffix: $(SRC_DIR)/trout-suffix.cpp
	$(CXX) $(CXXFLAGS) $^ -o $@

$(BIN_DIR)/%: $(SRC_DIR)/%.py
	cp $< $@

clean:
	rm -rf $(BIN_DIR)


sample: trout distance

distance: sketch
	mkdir -p sam/distance
	$(BIN_DIR)/trout-matrix sam/sketches/ > sam/distance/Actino16s.trout.distance

sketch: $(SAM_DATA) $(SAM_MARK)
	mkdir -p sam/sketches
	$(BIN_DIR)/trout-suffix sam/data/1_Actino16s.F.fq sam/data/act.lsh sam/sketches/1_Actino16s.F.trout.sketch
	$(BIN_DIR)/trout-suffix sam/data/2_Actino16s.F.fq sam/data/act.lsh sam/sketches/2_Actino16s.F.trout.sketch
	$(BIN_DIR)/trout-suffix sam/data/3_Actino16s.F.fq sam/data/act.lsh sam/sketches/3_Actino16s.F.trout.sketch
	$(BIN_DIR)/trout-suffix sam/data/4_Actino16s.F.fq sam/data/act.lsh sam/sketches/4_Actino16s.F.trout.sketch
	$(BIN_DIR)/trout-suffix sam/data/5_Actino16s.F.fq sam/data/act.lsh sam/sketches/5_Actino16s.F.trout.sketch
	$(BIN_DIR)/trout-suffix sam/data/6_Actino16s.F.fq sam/data/act.lsh sam/sketches/6_Actino16s.F.trout.sketch
	$(BIN_DIR)/trout-suffix sam/data/7_Actino16s.F.fq sam/data/act.lsh sam/sketches/7_Actino16s.F.trout.sketch
	$(BIN_DIR)/trout-suffix sam/data/8_Actino16s.F.fq sam/data/act.lsh sam/sketches/8_Actino16s.F.trout.sketch
	$(BIN_DIR)/trout-suffix sam/data/Mock3_Actino16s.F.fq sam/data/act.lsh sam/sketches/Mock3_Actino16s.F.trout.sketch

sample-clean:
	rm -rf sam/sketches/
	rm -rf sam/distance/


